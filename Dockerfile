FROM ubuntu:16.04
LABEL Description="This image is a clean build of casinocoin from it's github default branch"

RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list

RUN apt-get update && \
    apt-get install -y build-essential libtool autotools-dev autoconf libssl-dev git-core libboost-all-dev libdb4.8-dev libdb4.8++-dev libevent-dev libminiupnpc-dev pkg-config && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN git clone https://github.com/casinocoin/casinocoin.git /opt/casinocoin

RUN cd /opt/casinocoin/src && make -f makefile.unix && strip casinocoind
    
RUN cp /opt/casinocoin/src/casinocoind /usr/local/bin/ && \
    rm -rf /opt/casinocoin

RUN groupadd -r casinocoin && useradd -r -m -g casinocoin casinocoin
ENV CASINOCOIN_DATA /data
RUN mkdir $CASINOCOIN_DATA
COPY casinocoin.conf $CASINOCOIN_DATA/casinocoin.conf
RUN chown casinocoin:casinocoin $CASINOCOIN_DATA && \
    ln -s $CASINOCOIN_DATA /home/casinocoin/.casinocoin
USER casinocoin
VOLUME /data
EXPOSE 44664 44663
CMD ["/usr/local/bin/casinocoind", "-conf=/data/casinocoin.conf", "-server", "-txindex", "-printtoconsole"]